<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 * author:cdadia
 */

//This page creates various objects as required by Paypal SDK Classes that we are sending along with request URL.

use PayPal\Api\Amount;
use PayPal\Api\Payer;
use PayPal\Api\Payment;
use PayPal\Api\RedirectUrls;
use PayPal\Api\Transaction;

//Get all the credentials as required for sending payment request
require 'paypal_init.php';


//Set an exception to ensure product data is sent from index.php. 
if (empty($_POST['Samsung-S10'])) {
    throw new Exception('Product Data Not Set in the form in index.php. Not valid');
}

$payerObj = new Payer();
$payerObj->setPaymentMethod('paypal');

// Set an example data for the payment. Post Data fetched from index.php
$currency = $_POST["Currency"];
$amountPayable = $_POST['Samsung-S10'];
$invoiceNumber = uniqid();

$amountObj = new Amount();
$amountObj->setCurrency($currency)
    ->setTotal($amountPayable);

$transactionObj = new Transaction();
$transactionObj->setAmount($amountObj)
    ->setDescription('Thank you for your purchase.')
    ->setInvoiceNumber($invoiceNumber);

$redirectUrlsObj = new RedirectUrls();
$redirectUrlsObj->setReturnUrl($ppConfig['return_url'])
    ->setCancelUrl($ppConfig['cancel_url']);

$paymentObj = new Payment();
$paymentObj->setIntent('sale')
    ->setPayer($payerObj)
    ->setTransactions([$transactionObj])
    ->setRedirectUrls($redirectUrlsObj);

//Sends request using JSON
try {
    $paymentObj->create($apiContext);
} catch (Exception $e) {
    echo "<pre>";
    print_r($e);
    echo "</pre>";
    throw new Exception('Failed to create payment link');
}


//Goes to getApprovalLink method in Payment Class which contains the request URL in PaypalConstant : Approval URL
header('location:' . $paymentObj->getApprovalLink());
exit(1);


