<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 * author:cdadia
 */

use PayPal\Api\Payment;
use PayPal\Api\PaymentExecution;

require 'paypal_init.php';

if (empty($_GET['paymentId']) || empty($_GET['PayerID'])) {
    throw new Exception('The response is missing the paymentId and PayerID');
}
session_start();
$_SESSION['failed'] = $_GET;
$paymentId = $_GET['paymentId'];
$paymentObj = Payment::get($paymentId, $apiContext);

$executionObj = new PaymentExecution();
$executionObj->setPayerId($_GET['PayerID']);

try {
    // Take the payment
    $paymentObj->execute($executionObj, $apiContext);

    try {
        $db = new mysqli($dbConfig['host'], $dbConfig['username'], $dbConfig['password'], $dbConfig['name']);

        $paymentObj = Payment::get($paymentId, $apiContext);

        $data = [
            'transaction_id' => $paymentObj->getId(),
            'payment_amount' => $paymentObj->transactions[0]->amount->total,
            'payment_status' => $paymentObj->getState(),
            'invoice_id' => $paymentObj->transactions[0]->invoice_number
        ];
        if (addPayment($data) !== false && $data['payment_status'] === 'approved') {
            // Payment successfully added, redirect to the payment complete page.
            session_start();
            $_SESSION['response']   =   $data;
            header('location:success.php');
            exit(1);
        } else {
            // Payment failed
            echo "Payment Failed";
        }

    } catch (Exception $e) {
        // Failed to retrieve payment from PayPal
            echo "<pre>";
            print_r($e);
            echo "</pre>";
    }

} catch (Exception $e) {
    // Failed to take payment
    echo "<pre>";
    print_r($e);
    echo "</pre>";
}

//Add Payment to Database
function addPayment($data)
{
    global $db;
    if (is_array($data)) {
        print_r($data);
      
        $db->query('INSERT INTO `payments` (txn_id, payment_gross, payment_status,created_on)'
                . ' VALUES("'.$data['transaction_id'].'", '.$data['payment_amount'].', "'.$data['payment_status'].'", now())');
        return $db->insert_id;
    }

    return false;
}


